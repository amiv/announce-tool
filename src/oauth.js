/* Reference javascript client for simple AMIVAPI Oauth

More info on OAuth:
https://github.com/amiv-eth/amivapi/blob/master/docs/OAuth.md

See `index.js` for a usage example.

Note:
By simply editing the URL, the user can provide any token, not just required
ones. If you only use this token to send requests to the API, this is no
problem, since the API will reject invalid tokens.
*/

import queryString from 'query-string';
import { apiUrl, OAuthId } from 'config';

const ls = window.localStorage;

// Persist token and state for oauth-request
const session = {
  token: ls.getItem('OAuth-Token'),
  state: ls.getItem('OAuth-State'),
};


export function getToken() {
  return session.token;
}

export function logout() {
  session.token = '';
  session.state = '';
  ls.setItem('OAuth-Token', '');
  ls.setItem('OAuth-State', '');
}

// Redirect to OAuth landing page
// As uri for the redirect back, use the current location
export function login() {
  // Clear current session and save a random string for csrf protection
  logout();
  const state = Math.random().toString();
  ls.setItem('OAuth-State', state);

  const query = queryString.stringify({
    response_type: 'token',
    client_id: OAuthId,
    redirect_uri: window.location.origin,
    state,
  });

  // Redirect to AMIV api oauth page
  window.location.href = `${apiUrl}/oauth?${query}`;
}

// Extract token from query string automatically if state matches
const params = queryString.parse(window.location.search);
if (params.state && params.access_token && (params.state === session.state)) {
  session.token = params.access_token;
  ls.setItem('OAuth-Token', session.token);
}
