// Development Configuration
export const greeting = 'Hi';
export const name = 'Developer';

// AMIV API and OAuth.
// The development API allows redirect to localhost for the client 'Local Tool'
// You do not need to change this
export const apiUrl = 'https://api-dev.amiv.ethz.ch';
export const OAuthId = 'Local Tool';
