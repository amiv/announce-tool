# AMIV .js Template

A boilerplate containing essential build tools. It uses
[Webpack](https://webpack.js.org) and includes the
[Webpack Dev Server](https://github.com/webpack/webpack-dev-server) with
hot-reloading for easy development and [Babel](https://babeljs.io),
[UglifyJS](http://lisperator.net/uglifyjs/) and a optimized production config
for Webpack to build transpiled, minified `.js` files along with gzipped files.

Furthermore [ESLint](https://eslint.org), preconfigured to use the
Airbnb rules, is provided to check your code. (Feel free to adjust
`.eslintrc` to you needs)

Finally, a `Dockerfile` is included that uses a two-stage build to create
a final image that only contains the built files and the small
[http-server](https://www.npmjs.com/package/http-server), which can server
the files (and the gzipped versions) and nothing else, ready to deploy
in any cluster.

Contrary to other boilerplates, *only* build tools are included, and you
are free to choose whatever framework you like and get started quickly!

This version of the boilerplate is based on
[webpack-babel-eslint](https://github.com/NotSpecial/webpack-babel-eslint)
and contains additional files to support AMIV OAuth and the gitlab CI.

## Setup

Clone this repository and run `npm install` to install all dependencies.

([Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com) are required.)


## Commands

A few useful commands are configured in npm:

```
# Start the development server
npm start

# If the development server does not detect file changes, try starting as root
sudo npm start

# Lint your code
npm run lint

# Lint and fix errors if possible
npm run lint-fix

# Build your project
npm run build
```

## Configuration

In many situations, development and production environments require different
configurations. With
[Webpack Resolve](https://webpack.js.org/configuration/resolve/), this can be
handled easily. There are two configuration files, `config.js` and
`config.prod.js`, which you can adjust to your needs.

In your code, `require('config')` or `import * as config from 'config'`,
which will automatically resolve to `config.js` when using the development
server and to `config.prod.js` when building your project.

## Continuous Integration

The existing `.gitlab-ci.yml` configuration file contains three stages:

- **test**: Run tests, currently only eslint -- add your own if needed!
- **build**: Build the docker container and push it to the
  [Docker Hub](hub.docker.com)
- **deploy**: Trigger a service update in the AMIV Cluster.

You can add your own tests to the **test** stage, but should not change the
**build** or **deploy** stages. 

To enable building and deploying, a few steps are required:

### Configure a runner

You need to enable a runner for your project, which will work on CI jobs.
Under `Settings > CI/CD > Runners`, activate the AMIV Runner and you are
good to go.

### Configure build

Under `Settings > CI/CD > Secret Variables`, add the variable
`CI_REGISTRY_IMAGE` and set it to the appropriate repository on Docker Hub.
The account for AMIV images is called `amiveth`, therefore you should
use the following repository for your project: `amiveth/yourprojectname`.

The login credentials for the amiveth repository are saved in the AMIV
group on gitlab, you only need so set the name in your repository.

### Configure deploy

Before you can automatically update services, you need to manually
create them in the AMIV cluster. Contact the head of AMIV IT for this.
Afterwards, under `Settings > CI/CD > Secret Variables`, add the variable
`CI_DEPLOY_SERVICE` and set it to the name of your service in the AMIV
cluster.

## Acknowledgments

Many thanks go to Moritz Schneider, who created the initial
configuration for webpack!
