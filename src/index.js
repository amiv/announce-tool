// Go for it! :)

// Example for config usage, uses development/production config as needed
import { greeting, name } from 'config';
import { login, logout, getToken } from './oauth';

// OAuth automatically checks for a token, let's see if one was provided
// If there is a token, we are logged in!
const token = getToken();

const title = document.createElement('h1');
title.innerHTML = `${greeting}, ${name}`;

const main = document.createElement('p');
main.innerHTML = `You are${token ? ' ' : ' not '}logged in!`;

// Create a button for either login or logout (reload needed to see changes)
const button = document.createElement('button');
function logoutAndReload() {
  logout();
  window.location.reload();
}
button.addEventListener('click', token ? logoutAndReload : login);
button.innerHTML = token ? 'Logout' : 'Login';

document.body.appendChild(title);
document.body.appendChild(main);
document.body.appendChild(button);
