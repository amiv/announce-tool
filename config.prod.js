// Production Configuration
export const greeting = 'Hello';
export const name = 'Production';

// AMIV API and OAuth.
// You need to register a client id with the API, ask the head of IT for this
export const apiUrl = 'https://api.amiv.ethz.ch';
export const OAuthId = 'CHANGE THIS TO YOUR ID';
